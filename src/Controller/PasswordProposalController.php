<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PasswordProposalController extends AbstractController
{
    /**
     * @Route("/password/proposal", name="password_proposal")
     */
    public function index()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/PasswordProposalController.php',
        ]);
    }
}
