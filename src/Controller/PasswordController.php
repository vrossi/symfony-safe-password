<?php

namespace App\Controller;

use App\Entity\Password;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PasswordController extends AbstractController
{
    /**
     * @Route("/passwords", name="password_list")
     */
    public function list()
    {
	$entityManager = $this->getDoctrine()->getEntityManager();
	$passwords = $entityManager->getRepository(Password::class)->findAll();

        return $this->json($passwords);
    }

    /**
     * @Route("/passwords/{id}", name="password_list")
     */
    public function details(int $id)
    {
	$entityManager = $this->getDoctrine()->getEntityManager();
	$password = $entityManager->getRepository(Password::class)->find($id);

        return $this->json($password);
    }
}
